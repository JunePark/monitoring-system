# 그래프 패널

* 그래프 형태의 패널 만들기

1. 좌측하단의 Metric 탭에서 process_resident_memory_bytes 입력

2. 좌측하단의 Options 탭에서 Legend에 {{jobs}} 입력

3. 우측의 Axis 에서 Placement에 Left 선택, Label에 data/bytes 입력 

3. 우측의 Standard Options에서 Unit을 bytes(IEC) 로 선택

4. 우측의 Panel Options에서 Title에 Memory Usage 입력

![GraphPanel00](./images/GraphPanel-0000.png)

![GraphPanel01](./images/GraphPanel-0001.png)

![GraphPanel02](./images/GraphPanel-0002.png)

![GraphPanel03](./images/GraphPanel-0003.png)

___
.END OF GRAFANA