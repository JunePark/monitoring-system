# 단일상태 패널

* 하나의 시계열값만 표기하는 패널에서 레이블값 출력 하는 방법

1. 우측의 Visualization 탭에서 Stat 선택

2. 좌측하단의 Metric 탭에서 prometheus_tsdb_head_series 입력

3. 우측의 Stat styles에서 Graph mode를 Area에서 None으로 변경

4. 우측의 Panel Options에서 Title에 Prometheus Time Series 입력

![Singlestat00](./images/Singlestat-0000.png)

![Singlestat01](./images/Singlestat-0001.png)

![Singlestat02](./images/Singlestat-0002.png)

![Singlestat03](./images/Singlestat-0003.png)

___
.END OF GRAFANA