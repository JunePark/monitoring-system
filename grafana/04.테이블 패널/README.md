# 테이블 패널

## 테이블 형태의 패널 만들기

1. 좌측하단의 Metric 탭에서 rate(node_network_receive_byte_total[1m]) 입력

2. 좌측하단의 Options 탭에서 Type을 Range에서 Instant로 변경

3. 우측의 Standard Options에서 Unit을 bytes/sec(IEC) 로 선택

4. 우측의 Panel Options에서 Title에 Network traffic Received 입력

5. 좌측하단의 Tranform 탭에서 Organize fields 를 선택하여 보여지지 않을 컬럼을 선택

![TablePanel00](./images/TablePanel-0000.png)

![TablePanel01](./images/TablePanel-0001.png)

![TablePanel02](./images/TablePanel-0002.png)

![TablePanel03](./images/TablePanel-0003.png)


## 여러개의 메트릭값을 이용한 테이블 형태의 패널 만들기

![multi-table-0000](./images/multi-table-0000.png)

![multi-table-0001](./images/multi-table-0001.png)

![multi-table-0002](./images/multi-table-0002.png)

![multi-table-0003](./images/multi-table-0003.png)

![multi-table-0004](./images/multi-table-0004.png)

![multi-table-0005](./images/multi-table-0005.png)

![multi-table-0006](./images/multi-table-0006.png)

![multi-table-0007](./images/multi-table-0007.png)

![multi-table-0008](./images/multi-table-0008.png)



## 컬럼값 기준으로 피봇 테이블 만들기

![pivot-table-0000](./images/pivot-table-0000.png)

![pivot-table-0001](./images/pivot-table-0001.png)

![pivot-table-0002](./images/pivot-table-0002.png)

![pivot-table-0003](./images/pivot-table-0003.png)

![pivot-table-0004](./images/pivot-table-0004.png)

![pivot-table-0005](./images/pivot-table-0005.png)

![pivot-table-0006](./images/pivot-table-0006.png)


___
.END OF GRAFANA