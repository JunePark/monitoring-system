# 템플릿 변수 활용

* 템플릿 변수 활용한 패널 만들기

![Var00](./images/Var-0000.png)

![Var01](./images/Var-0001.png)

![Var02](./images/Var-0002.png)

![Var03](./images/Var-0003.png)

![Var04](./images/Var-0004.png)

![Var05](./images/Var-0005.png)

![Var06](./images/Var-0006.png)

![Var07](./images/Var-0007.png)

![Var08](./images/Var-0008.png)

![Var09](./images/Var-0009.png)

![Var10](./images/Var-0010.png)

![Var11](./images/Var-0011.png)

![Var12](./images/Var-0012.png)

![Var13](./images/Var-0013.png)

![Var14](./images/Var-0014.png)


___
.END OF GRAFANA