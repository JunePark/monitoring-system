# 직접 계측(direct instrumentation) 예제

## 0. 계획
* hadoop 계정으로 실행
* python3 으로 코드 작성
* 별도의 언급이 없는한 모든 명령은 hadoop03 에서 실행


## ~~1. 계정 추가 (필요없음)~~


## 2. 설치
* python 프로메테우스 클라이언트 라이브러리 설치
```
$ sudo apt install python3-pip
$ pip3 install prometheus_client
```

실행예)
```
Collecting prometheus_client
  Downloading prometheus_client-0.14.1-py3-none-any.whl (59 kB)
     |████████████████████████████████| 59 kB 3.0 MB/s 
Installing collected packages: prometheus-client
Successfully installed prometheus-client-0.14.1
```

## 3. Hello World 프로그램
* 프로그램 개요
![hello_world](./images/Hello_World.jpg)

* 3-1-example.py
```
import http.server
from prometheus_client import start_http_server

class MyHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b"Hello World")

if __name__ == "__main__":
    start_http_server(8000)
    server = http.server.HTTPServer(('0.0.0.0', 8001), MyHandler)
    server.serve_forever()
```
> [3-1-example.py](program/3-1-example.py)


## 4. 실행

```
$ python3 3-1-example.py
```
![direct_instrumentation00](./images/direct_instrumentation-0000.png)



## 5. 확인
* 포트 확인
```
$ netstat -an|egrep -i "8000|8001"
```
* 실행 예)
```
hadoop@hadoop03:~/$ netstat -an|egrep -i "8000|8001"
tcp        0      0 0.0.0.0:8000            0.0.0.0:*               LISTEN     
tcp        0      0 0.0.0.0:8001            0.0.0.0:*               LISTEN     
hadoop@hadoop03:~$ 
```
> NOTE. 8000, 8001 포트가 열려있는지 확인



## 6. 접속
* Web에서 metric 확인

![direct_instrumentation01](./images/direct_instrumentation-0001.png)
> 웹브라우저에서 hadoop03:8000 접속하면 metric 정보를 확인할수 있다.



## 7. prometheus 설정
* prometheus 설정 추가

(in hadoop03)
```
$ cd /opt/prometheus/
$ vi prometheus.yml
```

추가)
```
  - job_name: "example"
    static_configs:
      - targets: ["hadoop03:8000"]
```


## 8. prometheus 재시작
(in hadoop03)
```
$ sudo systemctl stop prometheus
$ sudo systemctl status prometheus
$ sudo systemctl start prometheus
$ sudo systemctl status prometheus
```


## 9. prometheus UI에서 metric 확인

* Request 전송
![direct_instrumentation02](./images/direct_instrumentation-0002.png)
![direct_instrumentation03](./images/direct_instrumentation-0003.png)
> Web을 이용해도 되고 curl 명령어를 이용해도 된다.

* Hello World 프로그램 확인
![direct_instrumentation04](./images/direct_instrumentation-0004.png)
> python_info 를 입력하면 하나의 결과가 생성된다.


___
.END OF DIRECT_INSTRUMENTATION