# 실행환경 설정

## 0. 계획
* Ubuntu, CentOS 계열 리눅스에서 Go, Java를 이용한 개발, 실행 환경 구성
* Java는 OpenJDK8 설치

## 1. Java 설치
* CentOS
```
$ rpm -qa|grep openjdk
(설치되어 있으면 삭제 필요)
$ sudo yum -y install java-1.8.0-openjdk-devel
$ rpm -qa|grep openjdk
$ java -version
```
> NOTE. rpm으로 설치


* Ubuntu
```
$ cd
$ mkdir download
$ cd download
$ wget https://github.com/adoptium/temurin8-binaries/releases/download/jdk8u322-b06/OpenJDK8U-jdk_x64_linux_hotspot_8u322b06.tar.gz
$ sudo tar -zxvf OpenJDK8U-jdk_x64_linux_hotspot_8u322b06.tar.gz -C /opt
$ sudo sh -c 'cd /opt/ && ln -s jdk8u322-b06 java'
$ /opt/java/bin/java -version
openjdk version "1.8.0_322"
OpenJDK Runtime Environment (Temurin)(build 1.8.0_322-b06)
OpenJDK 64-Bit Server VM (Temurin)(build 25.322-b06, mixed mode)
```

* 설정(공통)
```
$ sudo vi /etc/profile.d/java.sh

export JAVA_HOME=/opt/java
export PATH=$PATH:$JAVA_HOME/bin
export CLASSPATH=.
```
> NOTE. 적용후 재로그인

## 3. Maven 설치
```
$ cd ~/download/
$ wget https://dlcdn.apache.org/maven/maven-3/3.8.6/binaries/apache-maven-3.8.6-bin.tar.gz --no-check-certificate
$ sudo tar -C /opt -zxvf apache-maven-3.8.6-bin.tar.gz
$ cd /opt;sudo ln -s apache-maven-3.8.6 maven
$ sudo vi /etc/profile.d/maven.sh
```

```
export MAVEN_HOME=/opt/maven
export M3_HOME=$MAVEN_HOME
export PATH=$PATH:/opt/maven/bin
```
> NOTE. 적용후 재로그인

```
mvn -version
```
___
.END OF CONFIG