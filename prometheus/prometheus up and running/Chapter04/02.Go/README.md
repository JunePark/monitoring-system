# Go 언어를 이용한 메트릭 게시 프로그램 예제

## 0. 계획
* Go 언어를 이용한 개발, 실행

## 1. git clone
```
$ mkdir ~/work/
$ git clone https://github.com/prometheus-up-and-running/examples.git prometheus-up-and-running
```
> 이미 한 경우에는 할 필요없음.

## 2. 예제
* 4-6-example.go
```
package main

import (
  "log"
  "net/http"

  "github.com/prometheus/client_golang/prometheus"
  "github.com/prometheus/client_golang/prometheus/promauto"
  "github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
  requests = promauto.NewCounter(
    prometheus.CounterOpts{
      Name: "hello_worlds_total",
      Help: "Hello Worlds requested.",
    })
)

func handler(w http.ResponseWriter, r *http.Request) {
  requests.Inc()
  w.Write([]byte("Hello World"))
}

func main() {
  http.HandleFunc("/", handler)
  http.Handle("/metrics", promhttp.Handler())
  log.Fatal(http.ListenAndServe(":8000", nil))
}
```

or
```
package main

import (
  "log"
  "net/http"

  "github.com/prometheus/client_golang/prometheus"
  "github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
  requests = prometheus.NewCounter(
    prometheus.CounterOpts{
      Name: "hello_worlds_total",
      Help: "Hello Worlds requested.",
    })
)

func init() {
  prometheus.MustRegister(requests)
}

func handler(w http.ResponseWriter, r *http.Request) {
  requests.Inc()
  w.Write([]byte("Hello World"))
}

func main() {
  http.HandleFunc("/", handler)
  http.Handle("/metrics", promhttp.Handler())
  log.Fatal(http.ListenAndServe(":8000", nil))
}
```
> promauto 를 사용하지 않고 prometheus 를 사용(대신 init 함수에서 MustRegister를 이용하여 등록해 줘야함.)

## 3. 컴파일
* mod 파일 생성
```
$ cd ~/work/prometheus-up-and-running/4/
$ go mod init prometheus-up-and-running/4-6-example
```
> go.mod 파일이 생성됨.

* 패키지 다운로드
```
$ go mod tidy
```
> go.sum 파일이 생성되고 go.mod 파일이 수정됨.

* 빌드
```
$ go build
```

* 실행
```
$ ./4-6-example
```

![go00](./images/go-0000.png)

![go01](./images/go-0001.png)

![go02](./images/go-0002.png)

> http://192.168.126.178:8000 을 통해서는 hello_worlds_total 값이 +1 증가됨.

> http://192.168.126.178:8000/metrics 를 통해서는 단순 메트릭 게시

___
.END OF GO