# Java 언어를 이용한 메트릭 게시 프로그램 예제

## 0. 계획
* Java 언어를 이용한 개발, 실행
* maven 을 이용한 컴파일

## 1. git clone
```
$ mkdir ~/work/
$ git clone https://github.com/prometheus-up-and-running/examples.git prometheus-up-and-running
```
> 이미 한 경우에는 할 필요없음.

## 2. 예제
* prometheus-up-and-running/4-7-java-httpserver/src/main/java/io/robustperception/book_examples/java_httpserver/Example.java
```
import io.prometheus.client.Counter;
import io.prometheus.client.hotspot.DefaultExports;
import io.prometheus.client.exporter.HTTPServer;

public class Example {
  private static final Counter myCounter = Counter.build()
      .name("my_counter_total")
      .help("An example counter.").register();

  public static void main(String[] args) throws Exception {
    DefaultExports.initialize();
    HTTPServer server = new HTTPServer(8000);
    while (true) {
      myCounter.inc();
      Thread.sleep(1000);
    }
  }
}
```

## 2. 컴파일
```
$ cd ~/work/prometheus-up-and-running/4/4-7-java-httpserver
$ mvn compile
$ mvn package
$ ls -al targets/
$ java -jar target/java_httpserver-1.0-SNAPSHOT-jar-with-dependencies.jar 
```

* 실행예)
```
[centos@centos7 4-7-java-httpserver]$ pwd
/home/centos/work/prometheus-up-and-running/4/4-7-java-httpserver
[centos@centos7 4-7-java-httpserver]$ ll
합계 4
-rw-rw-r--. 1 centos centos 1954  8월 14 12:23 pom.xml
drwxrwxr-x. 3 centos centos   18  8월 14 12:23 src
[centos@centos7 4-7-java-httpserver]$ mvn compile
[INFO] Scanning for projects...
[INFO] 
[INFO] ---------< io.robustperception.book_examples:java_httpserver >----------
[INFO] Building java_httpserver 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-resources-plugin/2.6/maven-resources-plugin-2.6.pom
...
Downloaded from central: https://repo.maven.apache.org/maven2/log4j/log4j/1.2.12/log4j-1.2.12.jar (358 kB at 112 kB/s)
[INFO] Changes detected - recompiling the module!
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[INFO] Compiling 1 source file to /home/centos/work/prometheus-up-and-running/4/4-7-java-httpserver/target/classes
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  53.795 s
[INFO] Finished at: 2022-08-14T13:31:08+09:00
[INFO] ------------------------------------------------------------------------
[centos@centos7 4-7-java-httpserver]$ 
[centos@centos7 4-7-java-httpserver]$ mvn package
[INFO] Scanning for projects...
[INFO] 
[INFO] ---------< io.robustperception.book_examples:java_httpserver >----------
[INFO] Building java_httpserver 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-surefire-plugin/2.12.4/maven-surefire-plugin-2.12.4.pom
...
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-assembly-plugin/2.2-beta-5/maven-assembly-plugin-2.2-beta-5.jar (209 kB at 358 kB/s)
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ java_httpserver ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/centos/work/prometheus-up-and-running/4/4-7-java-httpserver/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ java_httpserver ---
[INFO] Changes detected - recompiling the module!
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[INFO] Compiling 1 source file to /home/centos/work/prometheus-up-and-running/4/4-7-java-httpserver/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ java_httpserver ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/centos/work/prometheus-up-and-running/4/4-7-java-httpserver/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ java_httpserver ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ java_httpserver ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/surefire/surefire-booter/2.12.4/surefire-booter-2.12.4.pom
...
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugin-tools/maven-plugin-annotations/3.1/maven-plugin-annotations-3.1.jar (14 kB at 10 kB/s)
[INFO] No tests to run.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ java_httpserver ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-archiver/2.5/maven-archiver-2.5.pom
...
Downloaded from central: https://repo.maven.apache.org/maven2/org/codehaus/plexus/plexus-utils/3.0/plexus-utils-3.0.jar (226 kB at 271 kB/s)
[INFO] Building jar: /home/centos/work/prometheus-up-and-running/4/4-7-java-httpserver/target/java_httpserver-1.0-SNAPSHOT.jar
[INFO] 
[INFO] --- maven-assembly-plugin:2.2-beta-5:single (make-assembly) @ java_httpserver ---
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-common-artifact-filters/1.1/maven-common-artifact-filters-1.1.pom
...
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/shared/maven-repository-builder/1.0-alpha-2/maven-repository-builder-1.0-alpha-2.jar (23 kB at 16 kB/s)
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/io.prometheus/ already added, skipping
[INFO] io/ already added, skipping
[INFO] io/prometheus/ already added, skipping
[INFO] io/prometheus/client/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/io.prometheus/ already added, skipping
[INFO] io/ already added, skipping
[INFO] io/prometheus/ already added, skipping
[INFO] io/prometheus/client/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/io.prometheus/ already added, skipping
[INFO] io/ already added, skipping
[INFO] io/prometheus/ already added, skipping
[INFO] io/prometheus/client/ already added, skipping
[INFO] io/prometheus/client/exporter/ already added, skipping
[INFO] Building jar: /home/centos/work/prometheus-up-and-running/4/4-7-java-httpserver/target/java_httpserver-1.0-SNAPSHOT-jar-with-dependencies.jar
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/io.prometheus/ already added, skipping
[INFO] io/ already added, skipping
[INFO] io/prometheus/ already added, skipping
[INFO] io/prometheus/client/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/io.prometheus/ already added, skipping
[INFO] io/ already added, skipping
[INFO] io/prometheus/ already added, skipping
[INFO] io/prometheus/client/ already added, skipping
[INFO] META-INF/MANIFEST.MF already added, skipping
[INFO] META-INF/ already added, skipping
[INFO] META-INF/maven/ already added, skipping
[INFO] META-INF/maven/io.prometheus/ already added, skipping
[INFO] io/ already added, skipping
[INFO] io/prometheus/ already added, skipping
[INFO] io/prometheus/client/ already added, skipping
[INFO] io/prometheus/client/exporter/ already added, skipping
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  49.239 s
[INFO] Finished at: 2022-08-14T14:10:09+09:00
[INFO] ------------------------------------------------------------------------
[centos@centos7 4-7-java-httpserver]$ ls -al target/
합계 92
drwxrwxr-x. 6 centos centos   184  8월 14 14:10 .
drwxrwxr-x. 4 centos centos    46  8월 14 13:31 ..
drwxrwxr-x. 2 centos centos     6  8월 14 14:10 archive-tmp
drwxrwxr-x. 2 centos centos    27  8월 14 14:09 classes
-rw-rw-r--. 1 centos centos 86023  8월 14 14:10 java_httpserver-1.0-SNAPSHOT-jar-with-dependencies.jar
-rw-rw-r--. 1 centos centos  2848  8월 14 14:09 java_httpserver-1.0-SNAPSHOT.jar
drwxrwxr-x. 2 centos centos    28  8월 14 14:09 maven-archiver
drwxrwxr-x. 3 centos centos    35  8월 14 13:31 maven-status
[centos@centos7 4-7-java-httpserver]$ java -jar target/java_httpserver-1.0-SNAPSHOT.jar
target/java_httpserver-1.0-SNAPSHOT.jar에 기본 Manifest 속성이 없습니다.
[centos@centos7 4-7-java-httpserver]$ java -jar target/java_httpserver-1.0-SNAPSHOT-jar-with-dependencies.jar 

```



![java00](./images/java-0000.png)

> http://192.168.126.178:8000, http://192.168.126.178:8000/metrics 을 통해서 동일하게 hello_worlds_total 값이 증가되는 값을 볼수 있음(1초 마다 1씩 증가.)

___
.END OF JAVA