# JavaServlet 이용한 메트릭 게시 프로그램 예제

## 0. 계획
* Java 언어를 이용한 개발, 실행
* maven 을 이용한 컴파일

## 1. git clone
```
$ mkdir ~/work/
$ git clone https://github.com/prometheus-up-and-running/examples.git prometheus-up-and-running
```
> 이미 한 경우에는 할 필요없음.

## 2. 예제
* prometheus-up-and-running/4/4-9-java-servlet/src/main/java/io/robustperception/book_examples/java_servlet/Example.java
```
import io.prometheus.client.Counter;
import io.prometheus.client.exporter.MetricsServlet;
import io.prometheus.client.hotspot.DefaultExports;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import java.io.IOException;


public class Example {
  static class ExampleServlet extends HttpServlet {
    private static final Counter requests = Counter.build()
        .name("hello_worlds_total")
        .help("Hello Worlds requested.").register();

    @Override
    protected void doGet(final HttpServletRequest req,
        final HttpServletResponse resp)
        throws ServletException, IOException {
      requests.inc();
      resp.getWriter().println("Hello World");
    }
  }

  public static void main(String[] args) throws Exception {
      DefaultExports.initialize();

      Server server = new Server(8000);
      ServletContextHandler context = new ServletContextHandler();
      context.setContextPath("/");
      server.setHandler(context);
      context.addServlet(new ServletHolder(new ExampleServlet()), "/");
      context.addServlet(new ServletHolder(new MetricsServlet()), "/metrics");

      server.start();
      server.join();
  }
}
```

* pom.xml
```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>io.robustperception.book_examples</groupId>
  <artifactId>java_servlet</artifactId>
  <packaging>jar</packaging>
  <version>1.0-SNAPSHOT</version>
  <url>http://maven.apache.org</url>
  <dependencies>
    <dependency>
      <groupId>io.prometheus</groupId>
      <artifactId>simpleclient</artifactId>
      <version>0.3.0</version>
    </dependency>
    <dependency>
      <groupId>io.prometheus</groupId>
      <artifactId>simpleclient_hotspot</artifactId>
      <version>0.3.0</version>
    </dependency>
    <dependency>
      <groupId>io.prometheus</groupId>
      <artifactId>simpleclient_servlet</artifactId>
      <version>0.3.0</version>
    </dependency>
    <dependency>
      <groupId>org.eclipse.jetty</groupId>
      <artifactId>jetty-servlet</artifactId>
      <version>8.2.0.v20160908</version>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.1</version>
        <configuration>
          <source>1.5</source>
          <target>1.5</target>
        </configuration>
      </plugin>
      <!-- Build a full jar with dependencies --> 
      <plugin>
        <artifactId>maven-assembly-plugin</artifactId>
        <configuration>
          <archive>
            <manifest>
              <mainClass>Example</mainClass>
            </manifest>
          </archive>
          <descriptorRefs>
            <descriptorRef>jar-with-dependencies</descriptorRef>
          </descriptorRefs>
        </configuration>
        <executions>
          <execution>
            <id>make-assembly</id>
            <phase>package</phase>
            <goals>
              <goal>single</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
</project>
```

## 2. 컴파일
```
$ cd ~/work/prometheus-up-and-running/4/4-9-java-servlet
$ mvn compile
$ mvn package
$ ls -al targets/
$ java -jar target/java_httpserver-1.0-SNAPSHOT-jar-with-dependencies.jar 
```

* 실행예)
```
[centos@centos7 4-9-java-servlet]$ pwd
/home/centos/work/prometheus-up-and-running/4/4-9-java-servlet
[centos@centos7 4-9-java-servlet]$ ll
합계 4
-rw-rw-r--. 1 centos centos 2112  8월 14 12:23 pom.xml
drwxrwxr-x. 3 centos centos   18  8월 14 12:23 src
drwxrwxr-x. 4 centos centos   41  8월 14 15:01 target
[centos@centos7 4-9-java-servlet]$ mvn compile
[INFO] Scanning for projects...
[INFO] 
[INFO] -----------< io.robustperception.book_examples:java_servlet >-----------
[INFO] Building java_servlet 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from central: https://repo.maven.apache.org/maven2/io/prometheus/simpleclient_servlet/0.3.0/simpleclient_servlet-0.3.0.pom
...
Downloaded from central: https://repo.maven.apache.org/maven2/org/eclipse/jetty/jetty-util/8.2.0.v20160908/jetty-util-8.2.0.v20160908.jar (288 kB at 193 kB/s)
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ java_servlet ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/centos/work/prometheus-up-and-running/4/4-9-java-servlet/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ java_servlet ---
[INFO] Changes detected - recompiling the module!
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[INFO] Compiling 1 source file to /home/centos/work/prometheus-up-and-running/4/4-9-java-servlet/target/classes
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  9.511 s
[INFO] Finished at: 2022-08-14T15:01:47+09:00
[INFO] ------------------------------------------------------------------------
[centos@centos7 4-9-java-servlet]$ ll target/
합계 0
drwxrwxr-x. 2 centos centos 63  8월 14 15:01 classes
drwxrwxr-x. 3 centos centos 35  8월 14 15:01 maven-status
[centos@centos7 4-9-java-servlet]$ mvn package
[INFO] Scanning for projects...
[INFO] 
[INFO] -----------< io.robustperception.book_examples:java_servlet >-----------
[INFO] Building java_servlet 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ java_servlet ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/centos/work/prometheus-up-and-running/4/4-9-java-servlet/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ java_servlet ---
[INFO] Changes detected - recompiling the module!
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[INFO] Compiling 1 source file to /home/centos/work/prometheus-up-and-running/4/4-9-java-servlet/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ java_servlet ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/centos/work/prometheus-up-and-running/4/4-9-java-servlet/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ java_servlet ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ java_servlet ---
[INFO] No tests to run.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ java_servlet ---
[INFO] Building jar: /home/centos/work/prometheus-up-and-running/4/4-9-java-servlet/target/java_servlet-1.0-SNAPSHOT.jar
[INFO] 
[INFO] --- maven-assembly-plugin:2.2-beta-5:single (make-assembly) @ java_servlet ---
[INFO] META-INF/MANIFEST.MF already added, skipping
...
[INFO] META-INF/maven/org.eclipse.jetty/ already added, skipping
[INFO] Building jar: /home/centos/work/prometheus-up-and-running/4/4-9-java-servlet/target/java_servlet-1.0-SNAPSHOT-jar-with-dependencies.jar
[INFO] META-INF/MANIFEST.MF already added, skipping
...
[INFO] META-INF/maven/org.eclipse.jetty/ already added, skipping
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  4.893 s
[INFO] Finished at: 2022-08-14T15:03:04+09:00
[INFO] ------------------------------------------------------------------------
[centos@centos7 4-9-java-servlet]$ ll target/
합계 1292
drwxrwxr-x. 2 centos centos       6  8월 14 15:03 archive-tmp
drwxrwxr-x. 2 centos centos      63  8월 14 15:03 classes
-rw-rw-r--. 1 centos centos 1318286  8월 14 15:03 java_servlet-1.0-SNAPSHOT-jar-with-dependencies.jar
-rw-rw-r--. 1 centos centos    3707  8월 14 15:03 java_servlet-1.0-SNAPSHOT.jar
drwxrwxr-x. 2 centos centos      28  8월 14 15:03 maven-archiver
drwxrwxr-x. 3 centos centos      35  8월 14 15:01 maven-status
[centos@centos7 4-9-java-servlet]$ java -jar target/java_servlet-1.0-SNAPSHOT.jar 
target/java_servlet-1.0-SNAPSHOT.jar에 기본 Manifest 속성이 없습니다.
[centos@centos7 4-9-java-servlet]$ java -jar target/java_servlet-1.0-SNAPSHOT-jar-with-dependencies.jar 
2022-08-14 15:04:45.236:INFO:oejs.Server:jetty-8.y.z-SNAPSHOT
2022-08-14 15:04:45.283:INFO:oejs.AbstractConnector:Started SelectChannelConnector@0.0.0.0:8000

```



![JavaServlet00](./images/JavaServlet-0000.png)


![JavaServlet01](./images/JavaServlet-0001.png)

> http://192.168.126.178:8000 을 통해서 metric 값을 +1 증가

> http://192.168.126.178:8000/metrics 를 통해서는 단순 메트릭 게시

___
.END OF JAVA