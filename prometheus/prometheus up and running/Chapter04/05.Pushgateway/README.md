# Pushgateway 설정 및 게시 프로그램 예제

## 0. 계획
* Pushgateway 설치
```
$ sudo useradd -m -u 9091 pushgateway

$ cd ~/download/
$ wget https://github.com/prometheus/pushgateway/releases/download/v1.4.3/pushgateway-1.4.3.linux-amd64.tar.gz
$ sudo tar -C /opt/ -zxvf pushgateway-1.4.3.linux-amd64.tar.gz
$ cd /opt;sudo ln -s pushgateway-1.4.3.linux-amd64 pushgateway
$ sudo chown -R pushgateway:pushgateway /opt/pushgateway-1.4.3.linux-amd64/
$ cd /opt;sudo chown -h pushgateway:pushgateway pushgateway

$ sudo vi /etc/systemd/system/pushgateway.service
```

```
[Unit]
Description=Prometheus Pushgateway
Documentation=https://prometheus.io/docs/practices/pushing/
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Restart=on-failure
ExecStart=/opt/pushgateway/pushgateway

[Install]
WantedBy=multi-user.target
```

* 실행
```
$ sudo systemctl daemon-reload
$ sudo systemctl start pushgateway
$ sudo systemctl status pushgateway
$ sudo systemctl enable pushgateway
```

* 체크
```
$ netstat -ntlp|grep 9091
```

* prometheus 설정 변경
```
$ sudo vi /opt/prometheus/prometheus.yml
```

```
scrape_configs:
  - job_name: pushgateway
    honor_labels: true
    static_configs:
      - targets: ["192.168.126.178:9091"]
```

* prometheus 재시작
```
$ sudo systemctl restart prometheus
```


## 1. git clone
```
$ mkdir ~/work/
$ git clone https://github.com/prometheus-up-and-running/examples.git prometheus-up-and-running
```
> 이미 한 경우에는 할 필요없음.

## 2. 예제
* 4-12-pushgateway.py
```
from prometheus_client import CollectorRegistry, Gauge, pushadd_to_gateway

registry = CollectorRegistry()
duration = Gauge('my_job_duration_seconds',
        'Duration of my batch job in seconds', registry=registry)
try:
    with duration.time():
        # Your code here. 
        pass

    # This only runs if there wasn't an exception. 
    g = Gauge('my_job_last_success_seconds',
            'Last time my batch job successfully finished', registry=registry)
    g.set_to_current_time()
finally:
    pushadd_to_gateway('localhost:9091', job='batch', registry=registry)
```

## 3. 실행
```
$ cd ~/work/prometheus-up-and-running/4/
$ python3 4-12-pushgateway.py 
```

![pushgateway00](./images/pushgateway-0000.png)

![pushgateway01](./images/pushgateway-0001.png)

![pushgateway02](./images/pushgateway-0002.png)

![pushgateway03](./images/pushgateway-0003.png)


> http://192.168.126.178:9091 을 통해서 확인 가능.

___
.END OF PUSHGATEWAY