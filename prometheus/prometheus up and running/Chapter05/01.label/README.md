# Label 개념

## 0. 계획


## 1. git clone
```
$ mkdir ~/work/
$ git clone https://github.com/prometheus-up-and-running/examples.git prometheus-up-and-running
```
> 이미 한 경우에는 할 필요없음.

## 2. 예제
* 5-1-example.py
```
import http.server
from prometheus_client import start_http_server, Counter

REQUESTS = Counter('hello_worlds_total',
        'Hello Worlds requested.',
        labelnames=['path'])

class MyHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        REQUESTS.labels(self.path).inc()
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b"Hello World")

if __name__ == "__main__":
    start_http_server(8000)
    server = http.server.HTTPServer(('0.0.0.0', 8001), MyHandler)
    server.serve_forever()
```

## 3. 실행
```
$ cd ~/work/prometheus-up-and-running/5/
$ python3 5-1-example.py
```


* 처음 상태

![label00](./images/label-0000.png)
![label01](./images/label-0001.png)

> metric 값이 처음에는 없음

* metric 값 변경

![label02](./images/label-0002.png)
![label03](./images/label-0003.png)
![label04](./images/label-0004.png)

> http://192.168.126.178:8001,  http://192.168.126.178:8001/foo 에 접속해서 metric 값을 변경

* metric 값 확인

![label05](./images/label-0005.png)

![label06](./images/label-0006.png)

> http://192.168.126.178:8000,  http://192.168.126.178:8000/metrics 에 접속해서 metric 값 확인


## 4. 다중 레이블 예제
* 5-2-example.py
```
import http.server
from prometheus_client import start_http_server, Counter

REQUESTS = Counter('hello_worlds_total',
        'Hello Worlds requested.',
        labelnames=['path','method'])

class MyHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        REQUESTS.labels(self.path, self.command).inc()
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b"Hello World")

if __name__ == "__main__":
    start_http_server(8000)
    server = http.server.HTTPServer(('0.0.0.0', 8001), MyHandler)
    server.serve_forever()
```

## 5. 실행
```
$ cd ~/work/prometheus-up-and-running/5/
$ python3 5-2-example.py
```

![label07](./images/label-0007.png)


___
.END OF LABEL