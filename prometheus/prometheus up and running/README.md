# 프로메테우스 오픈소스 모니터링 책

* Oreilly
ref) https://www.oreilly.com/library/view/prometheus-up/9781492034131/

* github
ref) https://github.com/prometheus-up-and-running

* pdf
ref) https://theswissbay.ch/pdf/Books/Computer%20science/prometheus_upandrunning.pdf